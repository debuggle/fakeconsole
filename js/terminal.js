var terminal = function(canvas, ctx) {

  console.log(window);

  // Setup the properties.
  this.ctx = ctx;
  this.canvas = canvas;
  this.events = {};
  this.commands = {};
  this.secret = {};
  this.buffer = [['>',' ']];
  this.line = 0;
  this.timer = 0;
  this.ctx = {};
  this.fileScope = '/home/';
  this.files = { '/' : { 'home': {} } };

  // Wire up the keydown events.
  var scope = this;
  window.onkeydown = function(evt) {
    scope.fireEvent("keydown", evt.key);
  }

  // Register some events.
  document.onkeydown = function(evt) {
    console.log(evt);
    //if ( evt.shiftKey ) return;
    if ( evt.ctrlKey ) return;
    if ( (evt.key || evt.keyIdentifier) == "F5" ) return;

    evt.preventDefault();
    var meta = evt.key ||evt.keyIdentifier;
    var key = evt.key || String.fromCharCode(evt.which);
    scope.buffer[scope.line] = (scope.buffer[scope.line]) || [];

    if ( meta  == "Backspace" || meta == "U+0008" || evt.which == 8 ) {
      if ( scope.buffer[scope.line].length > 2)
        scope.buffer[scope.line].pop();
    } else if ( meta  == "Enter" ) {
      var lineOfText = scope.buffer[scope.line].join("").substr(2);
      scope.printf("");
      scope.processCommand(lineOfText.toLowerCase());
    } else if ( meta  == "Shift" || meta == "U+00A0" ) {
      // Do nothing
    } else if ( meta == "U+0038" ) {
      scope.buffer[scope.line].push("*");
    } else if ( meta == "U+00DC" ) {
      scope.buffer[scope.line].push("|");
    } else if ( meta == "F12" ) {
      alert("Don't you DARE cheat.");
    } else if ( meta  == "Tab" || meta  == "Alt" || meta == "Control") {

    } else if ( (evt.key || evt.keyIdentifier) == "U+00BE") {
      // Some weirdness with chrome.
      scope.buffer[scope.line].push(".");
    } else {
      scope.buffer[scope.line].push(key.toLowerCase());
    }
  }

  // Print the default statement.
  var projectName = 'IYQ';
  this.printf("Welcome to the " + projectName + " Console");
  this.printf("Copyright (c) Debuggle 2015");
  this.printf("Please enter a command or type 'help' for more info.");
  this.printf("");

  // Register commands.
  this.registerCommand("help", function() {
    this.printf("The following commands are available: ");
    var array = [];
    for ( var prop in this.commands ) {
      array.push(prop);
    }

    function rightPad(txt, spaces) {
      var index = spaces - txt.length;
      var result = txt;
      for ( var i = 0; i < index; i++ )
        result += ' ';

      return result;
    }

    array.sort();
    for ( var i = 0; i < array.length; i++ ) {
      var i1 = i;
      var i2 = ++i;

      if ( i2 < array.length - 1)
        this.printf(" " + rightPad(array[i1],12) + array[i2] );
      else
        this.printf(" " + array[i1]);
    }
  });

  this.registerCommand("clear", function() {
    this.line = 0;
    this.buffer = [['>',' ']];
  });

  this.registerCommand("riddle", function(argument) {
    argument = argument[0];
    this.ctx.riddle = this.ctx.riddle || 0;
    var number = this.ctx.riddle || 0;

    if ( (argument || '').length == 0 ) {
      this.printf("Use: riddle [answer] to check your answer.");
      this.printf("");
    }

    if ( number == 0 ) {
      if ( argument == "map" ) {
        this.processCommand("clear");
        this.printf("Correct!");
        this.ctx.riddle++;
      } else if ( (argument || '').length > 0 ) {
        this.printf("Try again.");
      } else {
        this.printf("I am a passport to adventure.");
        this.printf("Through me, you can see the echo of time.");
        this.printf("Though I oft crinkle from use.");
        this.printf("What am I?");
        this.printf("");
      }

    }
  });

  this.registerCommand("pwd", function() {
    this.printf(this.fileScope);
  });

  this.registerCommand("ls", function(args) {
    var location = this.fileScope;
    var folder = this.getFolder(location);
    var scope = this;

    if ( folder == null ) return;

    // implement grep.
    if ( args && args[0] == '|') {
      if ( args[1] == 'grep' ) {
        var find = args[2];
        var reg = new RegExp('.' + find.replace('.', '\\.'));

        // Iterate.
        var stack = [[this.fileScope, folder]];
        do {
          var obj = stack.pop();
          var path = obj[0];
          var f = obj[1];

          if ( typeof(f) == typeof({})) {

            // Iterate over each property.
            for ( var prop in f ) {

              // Clean the path information a little.
              if ( prop[prop.length-1] == '/' )
                prop = prop.substr(0,prop.length-1);
              if ( prop[0] == '/' )
                prop = prop.substr(1);

              // Do the regex matching.
              if ( prop.match(reg) )
                this.printf(path + prop);

              stack.push([path + prop + "/", f[prop]]);
            }

          }
        } while ( stack.length > 0 );
        return;
      }
    }

    // Iterate over our structure.
    for ( var prop in folder ) {
      this.printf( " " + prop);
    }
  });

  this.registerCommand("rmdir", function(args) {
    args = args[0];
    var folder = this.getFolder(this.fileScope);
    delete folder[args];
  });

  this.registerCommand("mkdir", function(args) {
    args = args[0];
    var folder = this.getFolder(this.fileScope);
    folder[args] = {};
  });

  this.registerCommand("cat", function(args) {
    args = args[0];
    var folder = this.getFolder(this.fileScope);
    var file = {};

    if ( args[0] == '/' )
      file = this.getFolder(args);
    else
      file = folder[args];

    this.printf(file);
  });


  this.registerCommand("cd", function(args) {
    args = args[0];
    args = args || '';
    var temp = '';
    var pwd = this.fileScope;
    var scope = this;

    function up(path) {

      if ( path[path.length-1] == '/' )
        path = path.substr(0,path.length-1);

      for ( var i = path.length - 1; i >= 0; i-- ) {
        if ( path[i] == '/' )
          return path.substr(0,i+1);
      }

      return path;
    }

    if ( args[0] == '/' )
      pwd = '/';

    for ( var i = 0; i < args.length; i++ ) {
      // Try to parse it.
      var c1 = args[i];
      var c2 = '';

      // Look ahead
      if ( i + 1 < args.length )
        c2 = args[i+1];

      if ( c1 + c2 == '//' )
        continue;

      if ( c1 + c2 == "./" )
        continue;

      // Try to derrive the meaning.
      if ( c1 + c2 == ".." ) {
        pwd = up(pwd);
        i++;
        continue;
      } else {
        if ( pwd[pwd.length-1] == '/' && c1 == '/' )
          continue;
        pwd += c1;
      }
    }

    if ( pwd[pwd.length -1] != '/' )
      pwd = pwd + '/';

    if (this.validPath(pwd))
      this.fileScope = pwd;
    else if ( pwd.indexOf('.') >= 0 )
      this.printf("Use 'cat' to read files.");
    else
      this.printf("Directory does not exist.");
  });

  
  this.registerSecret("starfleet", function(args) {
    this.ctx.starfleet = this.ctx.starfleet || 0;

    if ( args == null || args == undefined || args.length == 0 ) {
      switch( this.ctx.starfleet ) {
        case 0:
          this.printf("Welcome to the starfleet challenge!");
          this.printf("Use: starfleet [answer] to verify your answer.");
          this.printf("");
          this.printf("What is the name of data's daughter?");
          break;
        case 1:
          this.printf("What does Lal mean?");
          break;
        case 2:
          this.printf("You've already solved this module.");
          this.printf("Go back to the map for more quests!");
          break;
      }

    } else {

      // Check which stage.
      switch( this.ctx.starfleet ) {
        case 0:
          if ( args.toString().toLowerCase() == "lal" ) {
            this.printf("Congratulations! Run the command again for the next stage.");
            this.ctx.starfleet++;
          } else {
            this.printf("Please try again.");
          }

          break;
        case 1:

          if ( args.toString().toLowerCase() == "love" ) {
            this.printf("You win!")
            this.ctx.starfleet++;
          } else {
            this.printf("Please try again.");
          }

          break;
      }

    }
  });


  // Start up the onDraw.
  // Start up the onUpdate.
  this.onDraw.call(this, ctx, canvas);
  this.onUpdate.call(this);
}

terminal.prototype.getParentFolder = function(path) {
  if ( path[path.length-1] == '/' )
    path = path.substr(0,path.length-1);

  if ( path.length == 1 ) return path;
  for ( var i = path.length - 1; i >= 0; i-- ) {
      if ( path[i] == '/' )
        return path.substr(0,i);
  }
  return path;
}

terminal.prototype.getFolder = function(location) {

  // Doing this thing.
  if ( location[0] != '/' )
    location[0] = '/';

  var folder = this.files['/'];
  var temp = '';
  for ( var i = 1; i < location.length; i++ ) {
    if( location[i] == '/' ) {
      if ( folder[temp] ) {
        folder = folder[temp];
      } else {
        return;
      }

      temp = '';
    } else {
      temp += location[i];
    }
  }

  if ( temp.length == 0 )
    if ( folder )
      return folder;

  if ( folder[temp] )
    return folder[temp];
  return null;
}

terminal.prototype.validPath = function(pwd) {
  if ( pwd.indexOf(".") > 0 ) return false;
  return this.getFolder(pwd) != null;
}

terminal.prototype.printf = function(text) {

  for ( var i = 0; i < text.length; i++ ) {
    var c = text[i];
    if ( c == '\n' ) {
      this.line++;
      this.buffer[this.line] = ['>', ' '];
    } else {
      this.buffer[this.line].push(text[i]);
    }
  }

  this.line++;
  this.buffer[this.line] = ['>',' '];
  if ( this.line > 25 ) {
    this.processCommand("clear");
  }
}

terminal.prototype.processCommand = function(cmd) {
  var cmds = (cmd || '').trim().split(' ');
  var args = [];
  var cmd = cmds[0].toLowerCase();

  for ( var i = 1; i < cmds.length; i++ ) {
    args.push(cmds[i]);
  }

  var scope = this;
  for ( var prop in this.commands ) {
    if ( prop == cmd ) {
      scope.commands[prop].call(scope, args);
      return;
    }
  }

  for ( var prop in this.secret ) {
    if ( prop == cmd ) {
      scope.secret[prop].call(scope, args);
    }
  }

}

terminal.prototype.registerSecret = function(cmd, method) {
  this.secret[cmd] = method;
}

terminal.prototype.registerCommand = function(cmd, method) {
  this.commands[cmd] = method;
}

terminal.prototype.addEventListener = function(evt, method) {
  this.events[evt] = method;
}

terminal.prototype.fireEvent = function(evt,arguments) {
  if ( this.events[evt] )
    this.events[evt].call(this, arguments);
}

terminal.prototype.onDraw = function(ctx, canvas) {

  ctx.clearRect(0,0,canvas.width,canvas.height);
  ctx.fillStyle = "#000000";
  ctx.fillRect(0,0,800,600);

  this.fireEvent("beforedraw");

  // Draw the buffer. Yep.
  ctx.fillStyle = "#00FF00";
  ctx.font = "bold 12px 'Source Code Pro'";
  for ( var r = 0; r <= this.line; r++) {
    // Reset to green.
    ctx.fillStyle = "#00FF00";
    for ( var i = 0; i < this.buffer[r].length; i++ ) {

      var c1 = this.buffer[r][i];
      var c2 = (i + 1 < this.buffer[r].length) ? this.buffer[r][i+1] : '';

      if ( c1 == '^' ) {
        if ( c2 == '0' ) ctx.fillStyle = "#fff"; // White
        else if ( c2 == '1' ) ctx.fillStyle = "#ff0000"; // Red
        else if ( c2 == '2' ) ctx.fillStyle = "#0000ff"; // Blue
        else if ( c2 == '3' ) ctx.fillStyle = "#800080"; // Purple
        else if ( c2 == '4' ) ctx.fillStyle = "#FFFF00"; // Yellow
        else if ( c2 == '5' ) ctx.fillStyle = "#FFA500"; // Orange
        else
          i--;

        i++;
        continue;
      }

      ctx.fillText(this.buffer[r][i], 20 + (7 * i), 20 + ( 20 * r));
    }
  }

  var blinkRate = 15;
  var cursorX = 12 + ((this.buffer[this.line].length + 1) * 7);
  var cursorY = 20 + (this.line * 20);
  this.timer++;

  if ( this.timer < blinkRate ) {
    // draw.
    ctx.fillText("|", cursorX, cursorY);
  } else if ( this.timer >= blinkRate && this.timer < (blinkRate*2) ) {
    // Do nothing.
  } else {
    this.timer = 0;
  }

  this.fireEvent("draw");

  // Redraw
  var scope = this;
  setTimeout(function() {
    scope.onDraw.call(scope, ctx, canvas)
  }, 35);
}

terminal.prototype.onUpdate = function() {

  // Reupdate.
  var scope = this;
  setTimeout(function() {
    scope.onUpdate.call(scope)
  }, 35);
}

module.exports = terminal;
