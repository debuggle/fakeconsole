// This is the primary entry point to the module.
var terminal = require('./terminal');

document.onreadystatechange = function () {
  if(document.readyState === "complete"){
      var canvas = document.getElementById('canvas');
      var ctx = canvas.getContext('2d');

      // Start the terminal. Yep.
      var t = new terminal(canvas, ctx);
  }
}
