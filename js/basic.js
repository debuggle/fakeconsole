var basic = function() {
  this.keywords = {};
  this.variables = {};
  this.register = [];
  this.terminal = null;

  // Register some keywords.
  this.registerKeyword("PRINT", function(args) {
    if ( this.terminal )
      this.terminal.printf.call(this.terminal, args[0]);
  });

  this.registerKeyword("LET", function(args) {
    this.variables[args[2]] = args[0];
  });
};

basic.prototype.setTerminal = function(terminal) {
  this.terminal = terminal;
}

basic.prototype.invokeKeyword = function(keyword, args) {
  if ( this.keywords[keyword] )
    this.keywords[keyword].call(this, args);
}

basic.prototype.registerKeyword = function(keyword, method) {
  this.keywords[keyword] = method;
}

basic.prototype.interpret = function(text) {

  // Always end with a \n to avoid having to do weird logic.
  text = text + "\n";
  console.log('interpreting ' + text);
  function contains(word, list) {
    for ( var i = 0; i < list.length; i++ )
      if (list[i] == word )
        return true;
    return false;
  }

  // parse out the language.
  var keywords = [ 'LET', 'IF', 'THEN', 'ELSE', 'FOR', 'TO', 'WHILE', 'WEND',
                   'DO', 'LOOP', 'REPEAT', 'UNTIL', 'GOTO', 'GOSUB', 'ON',
                   'DEF', 'INPUT','PRINT' ];

  var symbols = [ ':', '(', ')','[', ']', '=', '+', '-'];

  var isString = false;
  var temp = '';
  for ( var i = 0; i < text.length; i++ ) {
    // Get the current character.
    var c1 = text[i];

    // If we're a string, continue logging until we hit a closing string.
    if ( isString ) {
      if ( c1 != '"' ) {
        temp += c1;
        continue;
      } else {
        // Push it on to the stack.
        isString = false;
        this.register.push(temp);
        temp = '';
        continue;
      }
    }

    // Check if it's a string.
    if ( c1 == '"' ) {
      isString = true;
      continue;
    } else if ( c1 == ' ' ) {
      // We have a token.
      this.register.push(temp);
      temp = '';
      continue;
    } else if ( c1 == '\n' ) {
        // Register the remaining thing.
        this.register.push(temp);
        temp = '';

        // Process the stack.
        var args = [];
        var token = this.register.pop();

        do {
          if ( contains(token,keywords ) ) {
            this.invokeKeyword(token, args);
          } else {
              args.push(token);
          }

          token = this.register.pop();
        } while ( token != null );

    } else {
      temp += c1;
    }
  }

}

module.exports = basic;
